#!/bin/sh

set -e

( cd 81topng
  make
)
mkdir -p png
( cd png
  rm *.png || true
  ../81topng/81topng
)

vec8 -control control-semi81.csv png/*.png > semi81.svg
test -s semi81.svg

ttf8 -dir semi81 -control control-semi81.csv semi81.svg
f8name -dir semi81 -control control-semi81.csv
f8dsig -dir semi81
assfont -dir semi81

mkdir -p img

for a in $(chum semi81.ttf)
do hb-view --font-size=16 --margin=0 --unicodes=$a semi81.ttf > img/u$a.png
done

# to reconstruct HTML table in README
# python grid.py "$(chum semi81.ttf)" > table.html

plak --order code semi81.ttf |
  kitty icat

#!/usr/bin/env python

# https://docs.python.org/3.7/library/itertools.html
import itertools
import re
import sys


def engrid(us):
    s = sorted(us)

    print("<table><thead><tr>")
    print("<td/>")
    for d in range(16):
        print("<td>…%X</td>" % d, end="")
    print("\n</tr></thead>")

    # sort into rows that share all but the last hex digit
    for prefix, group in itertools.groupby(s, lambda x: x // 16):
        pref = "U+%03Xx" % prefix
        pref = "%7s" % pref
        group = list(group)

        print("<tr><th>%s</th>" % pref)
        for a in range(16):
            if not group or (group[0] % 16 != a):
                print("<td/>", end="")
            else:
                u, *group = group
                print("<td><img src='img/u%04X.png'/></td>" % u, end="")
        print("</tr>")
    print("</table>")


def main(argv=None):
    if argv is None:
        argv = sys.argv

    HEX_RE = r"[0-9a-fA-F]+"
    unistrings = re.findall(HEX_RE, argv[1])
    unicodes = [int(x, 16) for x in unistrings]
    engrid(unicodes)


if __name__ == "__main__":
    main()
